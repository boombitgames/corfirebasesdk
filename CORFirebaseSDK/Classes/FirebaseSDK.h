@interface FirebaseSDK : NSObject

+ (FirebaseSDK*)sharedManager;

-(void)FirebaseInit;

-(void)FirebaseLogEvent:(NSString*) eventName;

-(void)FirebaseLogEvent:(NSString*) eventName withParameters:(NSMutableDictionary*) parameters;

-(void)SetConsent:(bool) hasConsent;

-(void)SetUserID:(NSString*) userID;

-(void)SetUserPropertyString:(NSString*) propertyString forName:(NSString*) name;

-(void)FirebaseSetSessionTimeoutDuration:(NSTimeInterval) seconds;

-(NSString*)GetAppInstanceId;
@end
