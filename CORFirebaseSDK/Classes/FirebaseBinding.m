#import "FirebaseSDK.h"

// Converts C style string to NSString
#define GetStringParam( _x_ ) ( _x_ != NULL ) ? [NSString stringWithUTF8String:_x_] : [NSString stringWithUTF8String:""]
// Converts C style string to NSString as long as it isnt empty
#define GetStringParamOrNil( _x_ ) ( _x_ != NULL && strlen( _x_ ) ) ? [NSString stringWithUTF8String:_x_] : nil
// Converts NSString to C style string by way of copy (Mono will free it)
#define MakeStringCopy( _x_ ) ( _x_ != NULL && [_x_ isKindOfClass:[NSString class]] ) ? strdup( [_x_ UTF8String] ) : NULL

void StartFirebase(void)
{
    NSLog(@"FirebaseBinding :: StartFirebase");
    [[FirebaseSDK sharedManager] FirebaseInit];
}

void FirebaseLogEvent(char* eventName)
{
    [[FirebaseSDK sharedManager] FirebaseLogEvent:GetStringParam(eventName)];
}

void FirebaseLogEventWithParametersJson(char* eventName, char* parameters)
{
    NSString *attris = GetStringParam(parameters);
    
    NSError *error;
    
    NSMutableDictionary *dictionaryParams=[NSJSONSerialization JSONObjectWithData:[attris dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (!error)
    {
        [[FirebaseSDK sharedManager] FirebaseLogEvent:GetStringParam(eventName) withParameters:dictionaryParams];
    }
    else
    {
        NSLog(@"FirebaseBinding :: FirebaseLogEventWithParameters :: error with parsing params = %@", error.description);
    }
}

void FirebaseSetConsent(bool hasConesent)
{
    [[FirebaseSDK sharedManager] SetConsent:hasConesent];
}

void FirebaseSetUserID(char* userID)
{
    [[FirebaseSDK sharedManager] SetUserID:GetStringParam(userID)];
}

void FirebaseSetUserPropertyString(char* name, char* propertyString)
{
    [[FirebaseSDK sharedManager] SetUserPropertyString:GetStringParam(propertyString) forName:GetStringParam(name) ];
}

void FirebaseSetSessionTimeoutDuration(long seconds)
{
    NSLog(@"FirebaseBinding :: FirebaseSetSessionTimeoutDuration :: %ld", seconds);

    [[FirebaseSDK sharedManager] FirebaseSetSessionTimeoutDuration:seconds];
}

char* FirebaseGetAppInstanceId(void)
{
    NSString* instanceId=[[FirebaseSDK sharedManager] GetAppInstanceId];
    
    size_t size=[instanceId length]+1;
    
    void* heapCopy = malloc(size);
    memcpy(heapCopy, [instanceId UTF8String], size);
    
    return (char*)heapCopy;
}
