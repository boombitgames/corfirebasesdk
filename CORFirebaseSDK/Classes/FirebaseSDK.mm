#import "FirebaseSDK.h"
#import "Firebase.h"

@implementation FirebaseSDK

UIView *UnityGetGLView();
void UnitySendMessage( const char * className, const char * methodName, const char * param );

+ (FirebaseSDK*)sharedManager
{
    static FirebaseSDK *sharedSingleton;
    
    if( !sharedSingleton )
    {
        sharedSingleton = [[FirebaseSDK alloc] init];
    }
    
    return sharedSingleton;
}

-(void)FirebaseInit
{
    NSLog(@"FirebaseSDK :: FirebaseInit");
    
    [FIRApp configure];
}

-(void)FirebaseLogEvent:(NSString*) eventName
{
    NSLog(@"FirebaseSDK :: FirebaseLogEvent :: %@", eventName);

    [FIRAnalytics logEventWithName:eventName parameters:nil];
}

-(void)FirebaseLogEvent:(NSString*) eventName withParameters:(NSMutableDictionary*) parameters
{
    [FIRAnalytics logEventWithName:eventName parameters:parameters];
}

-(void)SetConsent:(bool) hasConsent
{
    NSLog(@"FirebaseSDK :: SetConsent :: %@", hasConsent ? @"TRUE":@"FALSE");

    [FIRAnalytics setAnalyticsCollectionEnabled:hasConsent];
}

-(void)SetUserID:(NSString*) userID
{
    [FIRAnalytics setUserID:userID];
}

-(void)SetUserPropertyString:(NSString*) propertyString forName:(NSString*) name
{
    [FIRAnalytics setUserPropertyString:propertyString forName:name];
}

-(void)FirebaseSetSessionTimeoutDuration:(NSTimeInterval) sessionTimeoutInterval
{
    NSLog(@"FirebaseSDK :: FirebaseSetSessionTimeoutDuration :: %f", sessionTimeoutInterval);

    [FIRAnalytics setSessionTimeoutInterval:sessionTimeoutInterval];
}

-(NSString*)GetAppInstanceId
{
    return FIRAnalytics.appInstanceID;
}
@end

