//
//  CORFIREBASESDKAppDelegate.h
//  CORFirebaseSDK
//
//  Created by Damian Grzybinski on 11/29/2021.
//  Copyright (c) 2021 Damian Grzybinski. All rights reserved.
//

@import UIKit;

@interface CORFIREBASESDKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
