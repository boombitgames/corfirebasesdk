//
//  CORFIREBASESDKViewController.m
//  CORFirebaseSDK
//
//  Created by Damian Grzybinski on 11/29/2021.
//  Copyright (c) 2021 Damian Grzybinski. All rights reserved.
//

#import "CORFIREBASESDKViewController.h"

@interface CORFIREBASESDKViewController ()

@end

@implementation CORFIREBASESDKViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
