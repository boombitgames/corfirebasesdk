//
//  main.m
//  CORFirebaseSDK
//
//  Created by Damian Grzybinski on 11/29/2021.
//  Copyright (c) 2021 Damian Grzybinski. All rights reserved.
//

@import UIKit;
#import "CORFIREBASESDKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CORFIREBASESDKAppDelegate class]));
    }
}
